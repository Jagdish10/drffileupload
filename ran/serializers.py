from rest_framework import serializers
from .models import UserDetails
class UserDetailSerializer(serializers.HyperlinkedModelSerializer):
    dp=serializers.ImageField(max_length=None,use_url=True)
    info=serializers.FileField(max_length=None,use_url=True)
    class Meta:
        model=UserDetails
        fields=('url','id','name','dp','info')


