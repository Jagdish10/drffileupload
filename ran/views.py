from django.shortcuts import render
from django.contrib.auth.models import User, Group
from ran.models import UserDetails
from rest_framework import viewsets,serializers
from .serializers import UserDetailSerializer
# Create your views here.
class UserDetailsViewSet(viewsets.ModelViewSet):
    queryset=UserDetails.objects.all()
    serializer_class=UserDetailSerializer
    